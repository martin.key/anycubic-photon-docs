## GENERAL TESTS

- Cost per kg
- Availability (distribuitors)

- Density (Anounced / Mesured - Need a density cup) 
- Color pre-cure (controlled light photo of a break sample?)
- Color post-cure (controlled light photo of a break sample?)
- Smell (subjective test - Check if the VOC sensosrs can detect a smelly resin from a non smelly one)

- Exposure sweet spot
- Exposure range

- Detail holding at Over/Center/Under exposure (select or create a photonsters test like ameratown for confirmation)
- Minimum support size @50u layer (develop a test)

- Washing solvent

## POST CURE TESTS

### BREAK TESTS (needs the OpenSource UTM machine)

- Kg at tensile break (Tensile strenght)
- Kg at 3 point break (Break force)
- Kg at compression failure


### OTHER TESTS

- Burn test (does it leave ash)
- Suitable for casting? (define test method)
- Heat resistance (how much heat does it take before failing integrity)


---
